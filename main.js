(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/about-us/about-us.component.html":
/*!**************************************************!*\
  !*** ./src/app/about-us/about-us.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\r\n<br>\r\n<div class=\"container-fluid\" style=\"background-color: white\">\r\n  <app-description></app-description>\r\n  <hr>\r\n</div>\r\n<br>\r\n<app-team></app-team>\r\n\r\n<br>\r\n<app-footer></app-footer>"

/***/ }),

/***/ "./src/app/about-us/about-us.component.scss":
/*!**************************************************!*\
  !*** ./src/app/about-us/about-us.component.scss ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/about-us/about-us.component.ts":
/*!************************************************!*\
  !*** ./src/app/about-us/about-us.component.ts ***!
  \************************************************/
/*! exports provided: AboutUsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutUsComponent", function() { return AboutUsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AboutUsComponent = /** @class */ (function () {
    function AboutUsComponent() {
    }
    AboutUsComponent.prototype.ngOnInit = function () {
    };
    AboutUsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-about-us',
            template: __webpack_require__(/*! ./about-us.component.html */ "./src/app/about-us/about-us.component.html"),
            styles: [__webpack_require__(/*! ./about-us.component.scss */ "./src/app/about-us/about-us.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], AboutUsComponent);
    return AboutUsComponent;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _about_us_about_us_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./about-us/about-us.component */ "./src/app/about-us/about-us.component.ts");
/* harmony import */ var _the_project_the_project_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./the-project/the-project.component */ "./src/app/the-project/the-project.component.ts");
/* harmony import */ var _homepage_homepage_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./homepage/homepage.component */ "./src/app/homepage/homepage.component.ts");
/* harmony import */ var _assignments_assignments_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./assignments/assignments.component */ "./src/app/assignments/assignments.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var routes = [
    { path: '', component: _homepage_homepage_component__WEBPACK_IMPORTED_MODULE_4__["HomepageComponent"] },
    { path: 'about-us', component: _about_us_about_us_component__WEBPACK_IMPORTED_MODULE_2__["AboutUsComponent"] },
    { path: 'assignments', component: _assignments_assignments_component__WEBPACK_IMPORTED_MODULE_5__["AssignmentsComponent"] },
    { path: 'the-project', component: _the_project_the_project_component__WEBPACK_IMPORTED_MODULE_3__["TheProjectComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes, { useHash: true })],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "h1{\r\n    font-weight: bold;\r\n    font-family: \"Arial Black\"\r\n}"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\r\n<router-outlet></router-outlet>\r\n\r\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'InNOVA.Util';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material_expansion__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/expansion */ "./node_modules/@angular/material/esm5/expansion.es5.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! angular-bootstrap-md */ "./node_modules/angular-bootstrap-md/esm5/angular-bootstrap-md.es5.js");
/* harmony import */ var _agm_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @agm/core */ "./node_modules/@agm/core/index.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./header/header.component */ "./src/app/header/header.component.ts");
/* harmony import */ var _footer_footer_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./footer/footer.component */ "./src/app/footer/footer.component.ts");
/* harmony import */ var _team_team_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./team/team.component */ "./src/app/team/team.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _about_us_about_us_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./about-us/about-us.component */ "./src/app/about-us/about-us.component.ts");
/* harmony import */ var _the_project_the_project_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./the-project/the-project.component */ "./src/app/the-project/the-project.component.ts");
/* harmony import */ var _homepage_homepage_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./homepage/homepage.component */ "./src/app/homepage/homepage.component.ts");
/* harmony import */ var _assignments_assignments_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./assignments/assignments.component */ "./src/app/assignments/assignments.component.ts");
/* harmony import */ var _description_description_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./description/description.component */ "./src/app/description/description.component.ts");
/* harmony import */ var _project_page_project_page_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./project-page/project-page.component */ "./src/app/project-page/project-page.component.ts");
/* harmony import */ var _project_description_project_description_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./project-description/project-description.component */ "./src/app/project-description/project-description.component.ts");
/* harmony import */ var _carousel_carousel_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./carousel/carousel.component */ "./src/app/carousel/carousel.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"],
                _header_header_component__WEBPACK_IMPORTED_MODULE_7__["HeaderComponent"],
                _footer_footer_component__WEBPACK_IMPORTED_MODULE_8__["FooterComponent"],
                _team_team_component__WEBPACK_IMPORTED_MODULE_9__["TeamComponent"],
                _about_us_about_us_component__WEBPACK_IMPORTED_MODULE_11__["AboutUsComponent"],
                _the_project_the_project_component__WEBPACK_IMPORTED_MODULE_12__["TheProjectComponent"],
                _homepage_homepage_component__WEBPACK_IMPORTED_MODULE_13__["HomepageComponent"],
                _assignments_assignments_component__WEBPACK_IMPORTED_MODULE_14__["AssignmentsComponent"],
                _homepage_homepage_component__WEBPACK_IMPORTED_MODULE_13__["HomepageComponent"],
                _description_description_component__WEBPACK_IMPORTED_MODULE_15__["DescriptionComponent"],
                _footer_footer_component__WEBPACK_IMPORTED_MODULE_8__["FooterComponent"],
                _project_page_project_page_component__WEBPACK_IMPORTED_MODULE_16__["ProjectPageComponent"],
                _project_description_project_description_component__WEBPACK_IMPORTED_MODULE_17__["ProjectDescriptionComponent"],
                _carousel_carousel_component__WEBPACK_IMPORTED_MODULE_18__["CarouselComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                angular_bootstrap_md__WEBPACK_IMPORTED_MODULE_4__["MDBBootstrapModule"].forRoot(),
                _app_routing_module__WEBPACK_IMPORTED_MODULE_10__["AppRoutingModule"],
                _angular_material_expansion__WEBPACK_IMPORTED_MODULE_2__["MatExpansionModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__["BrowserAnimationsModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_10__["AppRoutingModule"],
                _agm_core__WEBPACK_IMPORTED_MODULE_5__["AgmCoreModule"].forRoot({
                    apiKey: 'AIzaSyCzM879nfhIfRv4yxZtezabpXhO_Wo1Y3Q',
                    libraries: ['places']
                })
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/assignments.ts":
/*!********************************!*\
  !*** ./src/app/assignments.ts ***!
  \********************************/
/*! exports provided: Assignment, ASSIGNMENTS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Assignment", function() { return Assignment; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ASSIGNMENTS", function() { return ASSIGNMENTS; });
var Assignment = /** @class */ (function () {
    function Assignment() {
    }
    return Assignment;
}());

var ASSIGNMENTS = [
    {
        number: 1,
        name: 'Good and Bad Design',
        studentAssignments: [
            { number: 47502,
                content: 'assets\\assignments\\1\\47502\\Good-VS-Bad-Design.pptx' },
            { number: 47987,
                content: 'assets\\assignments\\1\\47987\\index.html' },
            { number: 48109,
                content: 'assets\\assignments\\1\\48109\\48109_Assignment1.pptx' },
            { number: 47278,
                content: 'assets\\assignments\\1\\47278\\Assignment1_47278.pptx' }
        ]
    }
];


/***/ }),

/***/ "./src/app/assignments/assignments.component.css":
/*!*******************************************************!*\
  !*** ./src/app/assignments/assignments.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#empty{\r\n  opacity: 0;\r\n}\r\n\r\nh2{\r\n  text-align: center;\r\n}\r\n\r\n.white-space{\r\n  height: 300px;\r\n}\r\n\r\na{\r\n  color: #066E3D;\r\n}\r\n"

/***/ }),

/***/ "./src/app/assignments/assignments.component.html":
/*!********************************************************!*\
  !*** ./src/app/assignments/assignments.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\r\n\r\n<!--<ul class=\"assignments\">\r\n  <li *ngFor=\"let a of assignments\">\r\n    <h3>Assignment {{a.number}}</h3>\r\n    {{a.studentAssignments}}\r\n\r\n</ul>\r\n </li>-->\r\n<br>\r\n<br>\r\n<h2>Assignments</h2>\r\n<hr>\r\n<br>\r\n\r\n<div class=\"row\">\r\n    <div class=\"col-md-2\"></div>\r\n    <div class=\"col-md-8 \">\r\n        <mat-accordion>\r\n            <mat-expansion-panel *ngFor=\"let a of assignments\">\r\n                <mat-expansion-panel-header>\r\n                    <mat-panel-title>\r\n                        Assignment {{a.number}} -<span id=\"empty\">-</span> <b>{{a.name}}</b>\r\n                    </mat-panel-title>\r\n\r\n                </mat-expansion-panel-header>\r\n                <p>\"Report: An example of a good interface design and one of a bad design.\"</p>\r\n                <ul>\r\n                    <li *ngFor=\"let studentAssignment of a.studentAssignments\">\r\n                        <a [href]=\"studentAssignment.content\">{{getStudent(studentAssignment.number).name}} | {{getStudent(studentAssignment.number).number}}</a>\r\n                        <!--<a [routerLink]=\"studentAssignment.endpoint\">\r\n                            {{getStudent(studentAssignment.number).name}}\r\n                        </a>-->\r\n                    </li>\r\n                </ul>\r\n            </mat-expansion-panel>\r\n        </mat-accordion>\r\n    </div>\r\n    <div class=\"col-md-2\"></div>\r\n</div>\r\n\r\n<br>\r\n<div class=\"row white-space\">\r\n\r\n</div>\r\n<app-footer></app-footer>"

/***/ }),

/***/ "./src/app/assignments/assignments.component.ts":
/*!******************************************************!*\
  !*** ./src/app/assignments/assignments.component.ts ***!
  \******************************************************/
/*! exports provided: AssignmentsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AssignmentsComponent", function() { return AssignmentsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _assignments__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../assignments */ "./src/app/assignments.ts");
/* harmony import */ var _student__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../student */ "./src/app/student.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AssignmentsComponent = /** @class */ (function () {
    function AssignmentsComponent() {
        this.assignments = _assignments__WEBPACK_IMPORTED_MODULE_1__["ASSIGNMENTS"];
    }
    AssignmentsComponent.prototype.ngOnInit = function () {
    };
    AssignmentsComponent.prototype.getStudent = function (number) {
        for (var _i = 0, STUDENTS_1 = _student__WEBPACK_IMPORTED_MODULE_2__["STUDENTS"]; _i < STUDENTS_1.length; _i++) {
            var student = STUDENTS_1[_i];
            if (student.number === number) {
                return student;
            }
        }
    };
    AssignmentsComponent.prototype.onClick = function (s) {
        this.studentAssign = s;
    };
    AssignmentsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-assignments',
            template: __webpack_require__(/*! ./assignments.component.html */ "./src/app/assignments/assignments.component.html"),
            styles: [__webpack_require__(/*! ./assignments.component.css */ "./src/app/assignments/assignments.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], AssignmentsComponent);
    return AssignmentsComponent;
}());



/***/ }),

/***/ "./src/app/carousel/carousel.component.css":
/*!*************************************************!*\
  !*** ./src/app/carousel/carousel.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#carousel-div{\r\n    height: 10% !important;\r\n}\r\n\r\nimg{\r\n    height: auto;\r\n    width: 25%;\r\n}\r\n\r\nh1{\r\n    font-size: x-large;\r\n}\r\n"

/***/ }),

/***/ "./src/app/carousel/carousel.component.html":
/*!**************************************************!*\
  !*** ./src/app/carousel/carousel.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<div id=\"carousel-div\" class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-md-4\"></div>\r\n    <div class=\"col-md-4\">\r\n      <mdb-carousel [isControls]=\"true\" [animation]=\"'slide'\">\r\n        <mdb-carousel-item>\r\n          <img class=\"d-block w-100\" src=\"../../assets/images/trucktris/TruckTris SILVER.jpg\" alt=\"First slide\">\r\n        </mdb-carousel-item>\r\n        <mdb-carousel-item>\r\n          <img class=\"d-block w-100\" src=\"../../assets/images/logos/InNovaUtil%20Circle.jpg\" alt=\"Second slide\">\r\n        </mdb-carousel-item>\r\n      </mdb-carousel>\r\n    </div>\r\n    <div class=\"col-md-4\"></div>\r\n  </div>\r\n\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/carousel/carousel.component.ts":
/*!************************************************!*\
  !*** ./src/app/carousel/carousel.component.ts ***!
  \************************************************/
/*! exports provided: CarouselComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarouselComponent", function() { return CarouselComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var CarouselComponent = /** @class */ (function () {
    function CarouselComponent() {
    }
    CarouselComponent.prototype.ngOnInit = function () {
    };
    CarouselComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-carousel',
            template: __webpack_require__(/*! ./carousel.component.html */ "./src/app/carousel/carousel.component.html"),
            styles: [__webpack_require__(/*! ./carousel.component.css */ "./src/app/carousel/carousel.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], CarouselComponent);
    return CarouselComponent;
}());



/***/ }),

/***/ "./src/app/description/description.component.css":
/*!*******************************************************!*\
  !*** ./src/app/description/description.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".center{\r\n    text-align: center;\r\n}\r\n\r\n.title{\r\n    text-align: left;\r\n\r\n    font-weight: bold;\r\n    padding-top: 30px;\r\n    padding-bottom: 10px;\r\n}\r\n\r\n.description{\r\n    text-align: justify;\r\n    padding-bottom: 20px;\r\n}\r\n\r\nagm-map {\r\n    height: 400px;\r\n}\r\n\r\n"

/***/ }),

/***/ "./src/app/description/description.component.html":
/*!********************************************************!*\
  !*** ./src/app/description/description.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<script async defer src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyCzM879nfhIfRv4yxZtezabpXhO_Wo1Y3Q&callback=initMap\"\r\n        type=\"text/javascript\"></script>\r\n\r\n<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-md-4\"></div>\r\n    <div class=\"col-md-4 center\">\r\n      <img src=\"../../assets/images/logos/Logo Innovautil.png\" style=\"max-width: 40%; height: auto\"/>\r\n      <!--<img src=\"../../assets/images/logos/InNovaUtil Circle.png\" style=\"max-width: 30%; height: auto\"/>--->\r\n    </div>\r\n    <div class=\"col-md-4\"></div>\r\n  </div>\r\n  <div class=\"row\">\r\n    <div class=\"col-md-4\"></div>\r\n    <div class=\"col-md-4\">\r\n\r\n      <h5 class=\"title\"><b>Who We Are</b></h5>\r\n    </div>\r\n    <div class=\"col-md-4\"></div>\r\n  </div>\r\n  <div class=\"row\">\r\n    <div class=\"col-md-4\"></div>\r\n    <div class=\"col-md-4\">\r\n      <p class=\"description\">\r\n        InNOVA.Util is formed by four Computer Science students at FCT NOVA. We joined forces in the context of the course Interação Pessoa-Máquina, lectured by Professor Teresa Romão, and we’ll work together throughout this first semester of the 2018/2019 school year.\r\n        <br>\r\n        <br>\r\n        Our aim is to solve some of the real world problems, mitigating some fault on our everyday lives. If a task is hard, there should be an app to make it easier.\r\n        <br>\r\n        <br>\r\n        This is our team website, where we’ll publish our information, projects, reports and news.\r\n      </p>\r\n    </div>\r\n    <div class=\"col-md-4\"></div>\r\n  </div>\r\n  <div class=\"row\">\r\n    <div class=\"col-md-2\"></div>\r\n    <div class=\"col-md-8\">\r\n      <agm-map [latitude]=\"lat\" [longitude]=\"lng\">\r\n        <agm-marker [latitude]=\"lat\" [longitude]=\"lng\"></agm-marker>\r\n      </agm-map>\r\n      <br>  <br>\r\n    </div>\r\n    <div class=\"col-md-2\"></div>\r\n  </div>\r\n</div>\r\n\r\n"

/***/ }),

/***/ "./src/app/description/description.component.ts":
/*!******************************************************!*\
  !*** ./src/app/description/description.component.ts ***!
  \******************************************************/
/*! exports provided: DescriptionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DescriptionComponent", function() { return DescriptionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var DescriptionComponent = /** @class */ (function () {
    function DescriptionComponent() {
        this.lat = 38.661150;
        this.lng = -9.205777;
    }
    DescriptionComponent.prototype.ngOnInit = function () {
    };
    DescriptionComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-description',
            template: __webpack_require__(/*! ./description.component.html */ "./src/app/description/description.component.html"),
            styles: [__webpack_require__(/*! ./description.component.css */ "./src/app/description/description.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], DescriptionComponent);
    return DescriptionComponent;
}());



/***/ }),

/***/ "./src/app/footer/footer.component.css":
/*!*********************************************!*\
  !*** ./src/app/footer/footer.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".footer-main{\r\n    background-color: whitesmoke;\r\n}\r\n\r\n.footer-links {\r\n    background-color: transparent;\r\n}\r\n\r\n.under-line{\r\n    margin-top:-1px;\r\n}\r\n\r\n.social-links{\r\n    text-align: center;\r\n}\r\n\r\n.green-line{\r\n    background-color: #066E3D;\r\n}\r\n\r\n.right{\r\n    text-align: right;\r\n}\r\n\r\n.container-padding{\r\n    padding-left: 20px;\r\n    padding-right: 20px;\r\n}"

/***/ }),

/***/ "./src/app/footer/footer.component.html":
/*!**********************************************!*\
  !*** ./src/app/footer/footer.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\r\n  <!-- Footer -->\r\n  <footer class=\"footer-main font-small pt-4 container-fluid container-padding\">\r\n\r\n    <!-- Footer Links -->\r\n    <div class=\"footer-links container-fluid text-center text-md-left\">\r\n\r\n      <!-- Grid row -->\r\n      <div class=\"row\">\r\n\r\n        <!-- Grid column -->\r\n        <div class=\"col-md-12 mt-md-0 mt-3 container-fluid right\">\r\n\r\n          <img src=\"../../assets/images/logos/Logo Innovautil.png\" height=\"88\" width=\"288\"/>\r\n\r\n        </div>\r\n\r\n\r\n\r\n      </div>\r\n      <!-- Grid row -->\r\n      <div class=\"row\">\r\n        <div class=\"col-md-12\">\r\n          <hr class=\"green-line\">\r\n        </div>\r\n      </div>\r\n      <div class=\"row under-line\">\r\n        <div class=\"col-md-11\">\r\n          <p>2018</p>\r\n        </div>\r\n        <div class=\"col-md-1\">\r\n          <a class=\"align-text-top col-xs-4 col-xs-offset-4 social-links\" href=\"https://bitbucket.org/innovautil/\" target=\"_blank\">\r\n            <img src=\"../../assets/images/bitbucket.png\" width=\"60\" height=\"31\"/>\r\n          </a>\r\n        </div>\r\n      </div>\r\n      <div style=\"height: 20px\" class=\"row\">\r\n        \r\n      </div>\r\n\r\n\r\n    </div>\r\n    <!-- Footer Links -->\r\n\r\n\r\n  </footer>\r\n  <!-- Footer -->\r\n</div>"

/***/ }),

/***/ "./src/app/footer/footer.component.ts":
/*!********************************************!*\
  !*** ./src/app/footer/footer.component.ts ***!
  \********************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! ./footer.component.html */ "./src/app/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.css */ "./src/app/footer/footer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/header/header.component.css":
/*!*********************************************!*\
  !*** ./src/app/header/header.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".menu-background{\r\n    background-color: white;\r\n}\r\n\r\n.nav-item a:hover{\r\n    background-color: #F1F1F1;\r\n\r\n}\r\n\r\n.active{\r\n    border-bottom: #066E3D solid 3px;\r\n    font-weight: bold;\r\n    background: white !important;\r\n    background-image: none;\r\n}\r\n\r\ndiv{\r\n     align-content: center;\r\n     text-align: center;\r\n     -ms-grid-row-align: center;\r\n         align-self: center;\r\n }\r\n\r\nnav{\r\n    align-content: center;\r\n    text-align: center;\r\n    -ms-grid-row-align: center;\r\n        align-self: center;\r\n    border-bottom: #066E3D solid 3px;\r\n}\r\n\r\na{\r\n    color:black !important;\r\n}"

/***/ }),

/***/ "./src/app/header/header.component.html":
/*!**********************************************!*\
  !*** ./src/app/header/header.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\r\n    <nav class=\"navbar navbar-expand-lg menu-background navbar-toggler navbar-nav\" >\r\n\r\n        <mdb-navbar-brand>\r\n            <a routerLink=\"/\">\r\n                <img class=\"navbar-brand d-print-inline-block \" src=\"../../assets/images/logos/InNovaUtil Circle.png\" height=\"100\"/>\r\n            </a>\r\n            <!--<a>InNOVA.Util</a>-->\r\n        </mdb-navbar-brand>\r\n        <links>\r\n            <ul class=\"navbar-nav mr-auto\">\r\n                <li class=\"nav-item\" >\r\n                    <a class=\"nav-link waves-light\" [routerLink]='home'\r\n                       [routerLinkActive]=\"['active']\"\r\n                       [routerLinkActiveOptions]=\"{ exact: true}\">\r\n                        Home\r\n                        <span class=\"sr-only\">(current)</span>\r\n                    </a>\r\n                </li>\r\n                <li class=\"nav-item\">\r\n                    <a class=\"nav-link waves-light\" [routerLink]='aboutUs'\r\n                       [routerLinkActive]=\"['active']\" mdbWavesEffect>\r\n                        About Us\r\n                    </a>\r\n                </li>\r\n                <li class=\"nav-item\">\r\n                    <a class=\"nav-link waves-light\" [routerLink]='assignments' [routerLinkActive]=\"['active']\" mdbWavesEffect>\r\n                        Assignments\r\n                    </a>\r\n                </li>\r\n                <li class=\"nav-item\" [routerLinkActive]=\"['active']\">\r\n                    <a class=\"nav-link waves-light\" [routerLink]='theProject' mdbWavesEffect>\r\n                        TrunkTris\r\n                    </a>\r\n                </li>\r\n            </ul>\r\n        </links>\r\n    </nav>\r\n</div>\r\n\r\n"

/***/ }),

/***/ "./src/app/header/header.component.ts":
/*!********************************************!*\
  !*** ./src/app/header/header.component.ts ***!
  \********************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HeaderComponent = /** @class */ (function () {
    function HeaderComponent() {
        this.home = '/';
        this.aboutUs = '/about-us';
        this.assignments = '/assignments';
        this.theProject = '/the-project';
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    HeaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.css */ "./src/app/header/header.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/homepage/homepage.component.css":
/*!*************************************************!*\
  !*** ./src/app/homepage/homepage.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/homepage/homepage.component.html":
/*!**************************************************!*\
  !*** ./src/app/homepage/homepage.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\r\n<div style=\"text-align:center\">\r\n  <app-header></app-header>\r\n  <br>\r\n  <br>\r\n  <h1>Welcome to <b>InNOVA.Util</b> Website</h1>\r\n  <br>\r\n  <hr>\r\n  <br>\r\n  <app-carousel></app-carousel>\r\n\r\n  <br/><br/>\r\n\r\n  <app-footer></app-footer>\r\n</div>\r\n\r\n"

/***/ }),

/***/ "./src/app/homepage/homepage.component.ts":
/*!************************************************!*\
  !*** ./src/app/homepage/homepage.component.ts ***!
  \************************************************/
/*! exports provided: HomepageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomepageComponent", function() { return HomepageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var HomepageComponent = /** @class */ (function () {
    function HomepageComponent() {
    }
    HomepageComponent.prototype.ngOnInit = function () {
    };
    HomepageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-homepage',
            template: __webpack_require__(/*! ./homepage.component.html */ "./src/app/homepage/homepage.component.html"),
            styles: [__webpack_require__(/*! ./homepage.component.css */ "./src/app/homepage/homepage.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], HomepageComponent);
    return HomepageComponent;
}());



/***/ }),

/***/ "./src/app/project-description/project-description.component.css":
/*!***********************************************************************!*\
  !*** ./src/app/project-description/project-description.component.css ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".center{\r\n    text-align: center;\r\n}\r\n\r\n.title{\r\n    text-align: left;\r\n\r\n    font-weight: bold;\r\n    padding-top: 30px;\r\n    padding-bottom: 10px;\r\n}\r\n\r\n.description{\r\n    text-align: justify;\r\n    padding-bottom: 20px;\r\n}\r\n\r\n#truck-tries{\r\n    max-width: 70%;\r\n    height: auto;\r\n}"

/***/ }),

/***/ "./src/app/project-description/project-description.component.html":
/*!************************************************************************!*\
  !*** ./src/app/project-description/project-description.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n<div class=\"container-fluid\">\r\n  <div class=\"row\">\r\n    <div class=\"col-md-4\"></div>\r\n    <div class=\"col-md-4 center\">\r\n      <img id=\"truck-tries\" src=\"../../assets/images/trucktris/TruckTris%20SILVER%20Straight.png\"/>\r\n    </div>\r\n    <div class=\"col-md-4\"></div>\r\n  </div>\r\n  <div class=\"row\">\r\n    <div class=\"col-md-4\"></div>\r\n    <div class=\"col-md-4\">\r\n\r\n      <h2 class=\"title\"><b>TruckTris</b></h2>\r\n      <hr>\r\n    </div>\r\n    <div class=\"col-md-4\"></div>\r\n  </div>\r\n  <div class=\"row\">\r\n    <div class=\"col-md-4\"></div>\r\n    <div class=\"col-md-4\">\r\n      <p class=\"description\">\r\n        Let’s face it: a truck’s trunk only has so much space. But then again, the cargo will not get packed by itself. When one is faced with the task of loading a truck, the first question is always the same: <i>“What goes where ?”</i>\r\n        <br><br>\r\n        TruckTris solves this problem, in a simple yet efficient way! One can visualize the cargo to load, it’s dimensions, sum it’s weight and plan the best way to pack it in the truck, according to the carrier’s preferences. It even elaborates the final delivery slip, simplifying the paperwork. All this, with one simple interface and even before arriving to the loading dock!\r\n        <br><br>\r\n        Your loading experience is now easy and can even be fun! It's like playing a game… like playing Tetris: <b>TruckTris</b> !\r\n      </p>\r\n    </div>\r\n    <div class=\"col-md-4\"></div>\r\n  </div>\r\n\r\n</div>\r\n<div class=\"row\">\r\n  <div class=\"col-md-5\"></div>\r\n  <div class=\"col-md-4\">\r\n    <img src=\"../../assets/images/trucktris/Trucks.jpg\" style=\"width: 70%; height: auto;\"/>\r\n  </div>\r\n  <div class=\"col-md-1\"></div>\r\n</div>\r\n\r\n"

/***/ }),

/***/ "./src/app/project-description/project-description.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/project-description/project-description.component.ts ***!
  \**********************************************************************/
/*! exports provided: ProjectDescriptionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectDescriptionComponent", function() { return ProjectDescriptionComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ProjectDescriptionComponent = /** @class */ (function () {
    function ProjectDescriptionComponent() {
    }
    ProjectDescriptionComponent.prototype.ngOnInit = function () {
    };
    ProjectDescriptionComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-project-description',
            template: __webpack_require__(/*! ./project-description.component.html */ "./src/app/project-description/project-description.component.html"),
            styles: [__webpack_require__(/*! ./project-description.component.css */ "./src/app/project-description/project-description.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ProjectDescriptionComponent);
    return ProjectDescriptionComponent;
}());



/***/ }),

/***/ "./src/app/project-page/project-page.component.html":
/*!**********************************************************!*\
  !*** ./src/app/project-page/project-page.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\r\n  project-page works!\r\n</p>\r\n"

/***/ }),

/***/ "./src/app/project-page/project-page.component.scss":
/*!**********************************************************!*\
  !*** ./src/app/project-page/project-page.component.scss ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/project-page/project-page.component.ts":
/*!********************************************************!*\
  !*** ./src/app/project-page/project-page.component.ts ***!
  \********************************************************/
/*! exports provided: ProjectPageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProjectPageComponent", function() { return ProjectPageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ProjectPageComponent = /** @class */ (function () {
    function ProjectPageComponent() {
    }
    ProjectPageComponent.prototype.ngOnInit = function () {
    };
    ProjectPageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-project-page',
            template: __webpack_require__(/*! ./project-page.component.html */ "./src/app/project-page/project-page.component.html"),
            styles: [__webpack_require__(/*! ./project-page.component.scss */ "./src/app/project-page/project-page.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], ProjectPageComponent);
    return ProjectPageComponent;
}());



/***/ }),

/***/ "./src/app/student.ts":
/*!****************************!*\
  !*** ./src/app/student.ts ***!
  \****************************/
/*! exports provided: Student, STUDENTS, EDGAR */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Student", function() { return Student; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "STUDENTS", function() { return STUDENTS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EDGAR", function() { return EDGAR; });
var Student = /** @class */ (function () {
    function Student() {
    }
    return Student;
}());

var STUDENTS = [
    { number: 47502, name: 'Ana Catarina Urgueira', email: 'a.urgueira@campus.fct.unl.pt' },
    { number: 47987, name: 'Edgar Silva', email: 'emda.silva@campus.fct.unl.pt' },
    { number: 48109, name: 'Pedro Deodato', email: 'p.deodato@campus.fct.unl.pt' },
    { number: 47278, name: 'Pedro Rodrigues', email: 'psa.rodrigues@campus.fct.unl.pt' }
];
var EDGAR = { number: 47987, name: 'Edgar Silva', email: 'emda.silva@campus.fct.unl.pt' };


/***/ }),

/***/ "./src/app/team/team.component.css":
/*!*****************************************!*\
  !*** ./src/app/team/team.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".circle{\r\n    border-radius: 50%;\r\n}\r\n\r\n.circle img{\r\n    border-radius: 50%;\r\n}\r\n\r\n.under{\r\n    margin-top: -20px;\r\n}\r\n\r\n.light{\r\n    font-weight: lighter;\r\n}\r\n\r\n.green-underline{\r\n    border-bottom: #066E3D solid 4px;\r\n    font-weight: bold;\r\n}\r\n\r\na{\r\n    color: #066E3D;\r\n}\r\n\r\n.text-center{\r\n    padding-top: 7px;\r\n}\r\n\r\n.text-padding{\r\n    padding-top: 5px;\r\n}\r\n\r\n.smooth:hover .smooth-max{\r\n    opacity: 0.5;\r\n}\r\n\r\n.smooth:hover .hidden{\r\n    opacity: 1 !important;\r\n}\r\n\r\n.hidden{\r\n    opacity: 0;\r\n    transition: .5s ease;\r\n    opacity: 0;\r\n    position: absolute;\r\n    top: 55%;\r\n    left: 50%;\r\n    -webkit-transform: translate(-50%, -50%);\r\n            transform: translate(-50%, -50%);\r\n    -ms-transform: translate(-50%, -50%);\r\n    text-align: center;\r\n}\r\n\r\n.hidden img{\r\n    background-color: white;\r\n}\r\n\r\n"

/***/ }),

/***/ "./src/app/team/team.component.html":
/*!******************************************!*\
  !*** ./src/app/team/team.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<section id=\"team\">\r\n  <div class=\"container-fluid\">\r\n    <h3 class=\"text-center\"><span class=\"green-underline\">Team</span></h3>\r\n    <br/>\r\n    <p class=\"text-center\">Practical Class <b>P4</b></p>\r\n    <p class=\"text-center under\">Interação Pessoa-Máquina</p>\r\n    <br/> <br/>\r\n    <div class=\"row\">\r\n      <div class=\"col-lg-2\">\r\n\r\n      </div>\r\n      <div class=\"col-lg-2 col-md-3 col-sm-3 col-xs-4 circle\">\r\n        <a href=\"https://www.linkedin.com/in/catarinaurgueira/\" target=\"_blank\">\r\n          <div class=\"smooth\">\r\n            <img class=\"smooth-max\" src=\"../../assets/images/team/Ana Catarina Urgueira Square.jpeg\" style=\"width:100%\"/>\r\n            <div class=\"hidden\">\r\n              <img src=\"../../assets/images/linkedin.png\" style=\"width:40%\"/>\r\n            </div>\r\n          </div>\r\n        </a>\r\n        <br/>\r\n        <p class=\"text-center text-padding\"><b>Ana Catarina Urgueira</b></p>\r\n        <p class=\"text-center under light\">Student nº: <b>47502</b></p>\r\n        <a href=\"mailto:a.urgueira@campus.fct.unl.pt\"><p class=\"text-center under\">Send an email</p></a>\r\n      </div>\r\n      <div class=\"col-lg-2 col-md-3 col-sm-3 col-xs-4 circle\">\r\n        <a href=\"https://www.linkedin.com/in/edgar-m-silva/\" target=\"_blank\" >\r\n          <div class=\"smooth\">\r\n            <img class=\"smooth-max\" src=\"../../assets/images/team/Edgar%20Silva%20Round.jpeg\" style=\"width:100%\"/>\r\n            <div class=\"hidden\">\r\n              <img src=\"../../assets/images/linkedin.png\" style=\"width:40%\"/>\r\n            </div>\r\n          </div>\r\n        </a>\r\n        <br/>\r\n        <p class=\"text-center text-padding\"><b>Edgar Silva</b></p>\r\n        <p class=\"text-center under light\">Student nº: <b>47987</b></p>\r\n        <a href=\"mailto:emda.silva@campus.fct.unl.pt\"><p class=\"text-center under\">Send an email</p></a>\r\n      </div>\r\n      <div class=\"col-lg-2 col-md-3 col-sm-3 col-xs-4 circle\">\r\n        <a href=\"https://www.linkedin.com/in/pedro-deodato/\" target=\"_blank\">\r\n          <div class=\"smooth\">\r\n            <img class=\"smooth-max\" src=\"../../assets/images/team/Pedro%20Deodato%20Square.jpeg\" style=\"width:100%\"/>\r\n            <div class=\"hidden\">\r\n              <img src=\"../../assets/images/linkedin.png\" style=\"width:40%\"/>\r\n            </div>\r\n          </div>\r\n        </a>\r\n        <br/>\r\n        <p class=\"text-center text-padding\"><b>Pedro Deodato</b></p>\r\n        <p class=\"text-center under light\">Student nº: <b>48109</b></p>\r\n        <a href=\"mailto:p.deodato@campus.fct.unl.pt\"><p class=\"text-center under\">Send an email</p></a>\r\n      </div>\r\n      <div class=\"col-lg-2 col-md-3 col-sm-3 col-xs-4 circle\">\r\n        <a href=\"https://www.linkedin.com/in/pedro-santos-rodrigues/\" target=\"_blank\" >\r\n          <div class=\"smooth\">\r\n            <img class=\"smooth-max\" src=\"../../assets/images/team/Pedro%20Rodrigues%20Square.jpeg\" style=\"width:100%\"/>\r\n            <div class=\"hidden\">\r\n              <img src=\"../../assets/images/linkedin.png\" style=\"width:40%\"/>\r\n            </div>\r\n          </div>\r\n        </a>\r\n        <br/>\r\n        <p class=\"text-center text-padding\"><b>Pedro Rodrigues</b></p>\r\n        <p class=\"text-center under light\">Student nº: <b>47278</b></p>\r\n        <a href=\"mailto:psa.rodrigues@campus.fct.unl.pt\"><p class=\"text-center under\">Send an email</p></a>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</section>\r\n<br>\r\n<br>"

/***/ }),

/***/ "./src/app/team/team.component.ts":
/*!****************************************!*\
  !*** ./src/app/team/team.component.ts ***!
  \****************************************/
/*! exports provided: TeamComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TeamComponent", function() { return TeamComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TeamComponent = /** @class */ (function () {
    function TeamComponent() {
    }
    TeamComponent.prototype.ngOnInit = function () {
    };
    TeamComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-team',
            template: __webpack_require__(/*! ./team.component.html */ "./src/app/team/team.component.html"),
            styles: [__webpack_require__(/*! ./team.component.css */ "./src/app/team/team.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], TeamComponent);
    return TeamComponent;
}());



/***/ }),

/***/ "./src/app/the-project/the-project.component.html":
/*!********************************************************!*\
  !*** ./src/app/the-project/the-project.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\r\n<br>\r\n<app-project-description></app-project-description>\r\n<br>\r\n<app-footer></app-footer>\r\n\r\n"

/***/ }),

/***/ "./src/app/the-project/the-project.component.scss":
/*!********************************************************!*\
  !*** ./src/app/the-project/the-project.component.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/the-project/the-project.component.ts":
/*!******************************************************!*\
  !*** ./src/app/the-project/the-project.component.ts ***!
  \******************************************************/
/*! exports provided: TheProjectComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TheProjectComponent", function() { return TheProjectComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var TheProjectComponent = /** @class */ (function () {
    function TheProjectComponent() {
    }
    TheProjectComponent.prototype.ngOnInit = function () {
    };
    TheProjectComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-the-project',
            template: __webpack_require__(/*! ./the-project.component.html */ "./src/app/the-project/the-project.component.html"),
            styles: [__webpack_require__(/*! ./the-project.component.scss */ "./src/app/the-project/the-project.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], TheProjectComponent);
    return TheProjectComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\eddy\Documents\innova.util_ws\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map